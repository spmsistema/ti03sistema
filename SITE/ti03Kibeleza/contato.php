<?php

	require_once ("vendor/PHPMailerAutoload.php");
   	$ok = 0;

try{
    if(isset($_POST["email"])){
		
        $assunto 	= "Site KiBeleza";
        $nome 		= $_POST["nome"];
        $email 		= $_POST["email"];
        $fone 		= $_POST["fone"];
        $mens	 	= $_POST["mens"];
		
		require_once("admin/class-contato.php");
		
		$contato = new Contato();
		$contato->nome	= $nome;
		$contato->email	= $email;
		$contato->fone	= $fone;
		$contato->mens	= $mens;
		
		$contato->Inserir();
        
                   
     
        $phpmail = new PHPMailer(); // Instânciamos a classe PHPmailer para poder utiliza-la  
		
        $phpmail->isSMTP(); // envia por SMTP
        
        $phpmail->SMTPDebug = 0;
        $phpmail->Debugoutput = 'html';
        
        $phpmail->Host = "smtp.gmail.com"; // SMTP servers         
        $phpmail->Port = 587; // Porta SMTP do GMAIL
        
        $phpmail->SMTPSecure = 'tls'; // Autenticação SMTP
        $phpmail->SMTPAuth = true; // Caso o servidor SMTP precise de autenticação   
        
        $phpmail->Username = "smpsistema@gmail.com"; // SMTP username         
        $phpmail->Password = "SENHA"; // SMTP password
		
        $phpmail->IsHTML(true);         
        
        $phpmail->setFrom($email, $nome); // E-mail do remetende enviado pelo method post  
                 
        $phpmail->addAddress("smpsistema@gmail.com", $assunto);// E-mail do destinatario/*  
        
        $phpmail->Subject = $assunto; // Assunto do remetende enviado pelo method post
                
        $phpmail->msgHTML(" Nome: $nome <br>
                            E-mail: $email <br>
                            Telefone: $fone <br>
                            Mensagem: $mens ");
						  						       
        $phpmail->AlrBody = "Nome: $nome \n
                            E-mail: $email \n
                            Telefone: $fone \n
                            Mensagem: $mens";
            
        if($phpmail->send()){ 
			
			$ok = 1;
            //echo "A Mensagem foi enviada com sucesso.";
			
        }else{
			$ok = 2;
            //echo "Não foi possível enviar a mensagem. Erro: " .$phpmail->ErrorInfo;
        }
		         
        // ############## RESPOSTA AUTOMATICA
        $phpmailResposta = new PHPMailer();        
        $phpmailResposta->isSMTP();
        
        $phpmailResposta->SMTPDebug = 0;
        $phpmailResposta->Debugoutput = 'html';
        
        $phpmailResposta->Host = "smtp.gmail.com";         
        $phpmailResposta->Port = 587;
        
        $phpmailResposta->SMTPSecure = 'tls';
        $phpmailResposta->SMTPAuth = true;   
        
        $phpmailResposta->Username = "smpsistema@gmail.com";         
        $phpmailResposta->Password = "SENHA";          
        $phpmailResposta->IsHTML(true);         
        
        $phpmailResposta->setFrom($email, $nome); // E-mail do remetende enviado pelo method post  
                 
        $phpmailResposta->addAddress($email, "Kibeleza");// E-mail do destinatario/*  
        
        $phpmailResposta->Subject = "Restosta - " .$assunto; // Assunto do remetende enviado pelo method post
                
        $phpmailResposta->msgHTML(" Nome: $nome <br>
                            Em breve daremos o retorno");
        
        $phpmailResposta->AlrBody = "Nome: $nome \n
                            Em breve daremos o retorno";
            
        $phpmailResposta->send();
        
    } // FECHAR O IF
    
}catch(Exception $e){
     Erro::tratarErro($e); 
}
    
?>


<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <meta charset="utf-8">
        <title>Clinica Kibeleza</title>
        <meta name="viewport" content="width=device-width">
        <!--CSS de reset das configurações do browser-->
        <link rel="stylesheet" type="text/css" href="css/reset.css">
        <!--CSS de carrossel-->
        <link rel="stylesheet" type="text/css" href="css/slick.css">
        <link rel="stylesheet" type="text/css" href="css/slick-theme.css">
        <!--CSS de animação-->
        <link rel="stylesheet" type="text/css" href="css/animate.css">	
		<!--LITY-->
        <link rel="stylesheet" type="text/css" href="css/lity.css">
        
        <!--CSS de estilo da página-->
        <link rel="stylesheet" href="css/estilo.css">

                <!--Icones favicon-->
        <link rel="icon" type="image/png" sizes="32x32" href="img/icon/favicon-32x32.png">
    </head>
    <body>
        <!--header - Cabeçalho do site-->
        <?php require_once("topo.php"); ?>
        
        <section class="map">
            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3659.0254647270517!2d-46.434050784408164!3d-23.495592265053293!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x94ce63dda7be6fb9%3A0xa74e7d5a53104311!2sSenac+S%C3%A3o+Miguel+Paulista!5e0!3m2!1spt-BR!2sbr!4v1562247662760!5m2!1spt-BR!2sbr" width="100%" height="500" frameborder="0" style="border:0" allowfullscreen></iframe>
        </section>
		
		<section class="site contato">
			<article>
			<h2>Formulário de contato</h2>
			
			<form action="#" method="post" class="form">
				<div>
					<input type="text" name="nome" placeholder="Digitar o nome: " required>
				</div>
				<div>
					<input type="email" name="email" placeholder="Digite seu e-mail: " required>
				</div>
				<div>
					<input type="tel" name="fone" placeholder="Digite seu telefone: ">
				</div>
				<div>
					<textarea name="mens" placeholder="Digite sua mensagem: " cols="5" rows="10"></textarea>
				</div>
				<div>
					<?php
						if($ok == 1){
							echo("<span>A Mensagem foi enviada com sucesso.</span>");
							
						}else if($ok == 2){
							echo("<span>Não foi possível enviar a mensagem.</span>");
						}			
					
					?>
					<input type="submit" value="ENVIAR">
				</div>
			</form>
			
			</article> <!-- Fim Article Form -->
			<article>
				<h2>Dados Salão KiBeleza!</h2>
				<ul>
					<li>Telefone: 5698-6598</li>
					<li>WhatsApp: 96886-6886</li>
					<li>End: Av. São Miguel, 2525 - São Miguel</li>
					<li>E-mail: contato@kibeleza.com.br</li>
					<li>Site: www.kibeleza.com.br</li>
				</ul>
			</article>
		</section>    
       
        
        <section class="galeria wow fadeInUpBig"><!-- GALERIA INSTA-->
            <div id="instafeed" class="instafeed"></div>
        </section><!-- FIM GALERIA INSTA-->

        <?php require_once("rodape.php"); ?>

        
    </body>
    <script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>
    <script type="text/javascript" src="js/slick.js"></script>
    <script type="text/javascript" src="js/wow.js"></script>
	<script type="text/javascript" src="js/lity.js"></script>
    <script type="text/javascript" src="js/animacao.js"></script>
    <script src="https://cdn.shopify.com/s/files/1/0683/5883/t/3/assets/instafeed.min.js"></script>
    <script src="https://cdn.jsdelivr.net/jquery.slick/1.3.15/slick.min.js"></script>
    <script src="js/instafeed.js"></script>
</html>