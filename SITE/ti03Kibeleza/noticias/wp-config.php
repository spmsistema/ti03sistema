<?php
/**
 * As configurações básicas do WordPress
 *
 * O script de criação wp-config.php usa esse arquivo durante a instalação.
 * Você não precisa usar o site, você pode copiar este arquivo
 * para "wp-config.php" e preencher os valores.
 *
 * Este arquivo contém as seguintes configurações:
 *
 * * Configurações do MySQL
 * * Chaves secretas
 * * Prefixo do banco de dados
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/pt-br:Editando_wp-config.php
 *
 * @package WordPress
 */

// ** Configurações do MySQL - Você pode pegar estas informações com o serviço de hospedagem ** //
/** O nome do banco de dados do WordPress */
define( 'DB_NAME', 'noticiasdois' );

/** Usuário do banco de dados MySQL */
define( 'DB_USER', 'root' );

/** Senha do banco de dados MySQL */
define( 'DB_PASSWORD', '' );

/** Nome do host do MySQL */
define( 'DB_HOST', 'localhost' );

/** Charset do banco de dados a ser usado na criação das tabelas. */
define( 'DB_CHARSET', 'utf8mb4' );

/** O tipo de Collate do banco de dados. Não altere isso se tiver dúvidas. */
define('DB_COLLATE', '');

/**#@+
 * Chaves únicas de autenticação e salts.
 *
 * Altere cada chave para um frase única!
 * Você pode gerá-las
 * usando o {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org
 * secret-key service}
 * Você pode alterá-las a qualquer momento para invalidar quaisquer
 * cookies existentes. Isto irá forçar todos os
 * usuários a fazerem login novamente.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'hVqD:-7(Zsf?kFIw}[[@?5)akl Z(UskK[E-U7>L0OP8K{[)?(d{Yo6azl6^vm:}' );
define( 'SECURE_AUTH_KEY',  '}Jd%/L%UYW[Idhg[;,^G7KrhY1%({C0Jd?D|FkhcGy:,Sr5afse3#{_M+/z/gbYk' );
define( 'LOGGED_IN_KEY',    '%i6|i(|g?HB5Eqr94O>qf?X;C5Ww#}J)l @e=^0EWy1}W2i^=Z)u5y9J,:3. 5r#' );
define( 'NONCE_KEY',        '7~KFb]CADT|19IWuaN.q%}83 tGXP6tdBXd<NFUDG0Q8M<S$`TF(GD4^#G?I@!Z$' );
define( 'AUTH_SALT',        '/Qta 7XK9Gr5!Y~2v@dzw+1rn>_5g;|W7:N?x6I?T`<b:my;6$vE;Elo wFj@Khu' );
define( 'SECURE_AUTH_SALT', '<X@?_a6o/9ZqZLrp].xgW#]2S?zH}]B;_bw;[$F2$2a8EQbBTVx-Iod/!oWM_g&t' );
define( 'LOGGED_IN_SALT',   'CO&B{VSD QbI0(NO*(L4W2cx*^DR8jo=T L7RL7cBR^Wyx+; {61mGAHlb7M1exw' );
define( 'NONCE_SALT',       'O4z1b$)/(MD3khh+^F97=wvF0`Jp2[O59:`9*xVCO!PW*m~?l*^b-r:Mx%b{5m5R' );

/**#@-*/

/**
 * Prefixo da tabela do banco de dados do WordPress.
 *
 * Você pode ter várias instalações em um único banco de dados se você der
 * um prefixo único para cada um. Somente números, letras e sublinhados!
 */
$table_prefix = 'wp_';

/**
 * Para desenvolvedores: Modo de debug do WordPress.
 *
 * Altere isto para true para ativar a exibição de avisos
 * durante o desenvolvimento. É altamente recomendável que os
 * desenvolvedores de plugins e temas usem o WP_DEBUG
 * em seus ambientes de desenvolvimento.
 *
 * Para informações sobre outras constantes que podem ser utilizadas
 * para depuração, visite o Codex.
 *
 * @link https://codex.wordpress.org/pt-br:Depura%C3%A7%C3%A3o_no_WordPress
 */
define('WP_DEBUG', false);

/* Isto é tudo, pode parar de editar! :) */

/** Caminho absoluto para o diretório WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Configura as variáveis e arquivos do WordPress. */
require_once(ABSPATH . 'wp-settings.php');
