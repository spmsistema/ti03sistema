<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <meta charset="utf-8">
        <title>Clinica Kibeleza</title>
        <meta name="viewport" content="width=device-width">
        <!--CSS de reset das configurações do browser-->
        <link rel="stylesheet" type="text/css" href="css/reset.css">
        <!--CSS de carrossel-->
        <link rel="stylesheet" type="text/css" href="css/slick.css">
        <link rel="stylesheet" type="text/css" href="css/slick-theme.css">
        <!--CSS de animação-->
        <link rel="stylesheet" type="text/css" href="css/animate.css">	
		<!--LITY-->
        <link rel="stylesheet" type="text/css" href="css/lity.css">
        
        <!--CSS de estilo da página-->
        <link rel="stylesheet" href="css/estilo.css">

                <!--Icones favicon-->
        <link rel="icon" type="image/png" sizes="32x32" href="img/icon/favicon-32x32.png">
    </head>
    <body>
        <!--header - Cabeçalho do site-->
        <?php require_once("topo.php"); ?>
        <?php require_once("banner.php"); ?>
        
    
        <section class="servico wow fadeInUpBig" ><!--DESTAQUE-->
            <div class="site">
                <h2>Serviços</h2>
                <hr>
                <!-- Grupo Serviços -->
                
                <?php require_once("servico.php"); ?>

                <!-- Fim Grupo Serviços -->
            </div>	
        </section><!--fim destque-->
       
        <section class="depoimento wow fadeInUpBig"><!-- DEPOIMENTO -->
			<div class="site">
                <article>
					<p>Lorem Ipsum é simplesmente uma simulação de texto da indústria tipográfica e de impressos. Lorem Ipsum é simplesmente uma simulação de texto da indústria tipográfica e de impressos.</p>
					<h2>Nome Autor</h2>
				</article>
			</div>
        </section><!-- FIM DEPOIMENTO -->
        
        <section class="galeria wow fadeInUpBig"><!-- GALERIA INSTA-->
            <div id="instafeed" class="instafeed"></div>
        </section><!-- FIM GALERIA INSTA-->

        <?php require_once("rodape.php"); ?>

        
    </body>
    <script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>
    <script type="text/javascript" src="js/slick.js"></script>
    <script type="text/javascript" src="js/wow.js"></script>
	<script type="text/javascript" src="js/lity.js"></script>
    <script type="text/javascript" src="js/animacao.js"></script>
    <script src="https://cdn.shopify.com/s/files/1/0683/5883/t/3/assets/instafeed.min.js"></script>
    <script src="https://cdn.jsdelivr.net/jquery.slick/1.3.15/slick.min.js"></script>
    <script src="js/instafeed.js"></script>
</html>