<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <meta charset="utf-8">
        <title>Clinica Kibeleza</title>
        <meta name="viewport" content="width=device-width">
        <!--CSS de reset das configurações do browser-->
        <link rel="stylesheet" type="text/css" href="css/reset.css">
        <!--CSS de carrossel-->
        <link rel="stylesheet" type="text/css" href="css/slick.css">
        <link rel="stylesheet" type="text/css" href="css/slick-theme.css">
        <!--CSS de animação-->
        <link rel="stylesheet" type="text/css" href="css/animate.css">	
		<!--LITY-->
        <link rel="stylesheet" type="text/css" href="css/lity.css">
        
        <!--CSS de estilo da página-->
        <link rel="stylesheet" href="css/estilo.css">

                <!--Icones favicon-->
        <link rel="icon" type="image/png" sizes="32x32" href="img/icon/favicon-32x32.png">
    </head>
    <body>
        <!--header - Cabeçalho do site-->
        <?php require_once("topo.php"); ?>
        <?php require_once("banner.php"); ?>
        
        
        <section class="sobre wow fadeInUpBig"><!-- SOBRE -->
            <div class="site">
                <article>
                    <h2>Lorem Ipsum</h2>
                    <p>Lorem Ipsum é simplesmente uma simulação de texto da indústria tipográfica e de impressos, e vem sendo utilizado desde o século XVI, quando um impressor desconhecido pegou uma bandeja de tipos e os embaralhou para fazer um livro de modelos de tipos. Lorem Ipsum sobreviveu não só a cinco séculos, como também ao salto para a editoração eletrônica, permanecendo essencialmente inalterado. Se popularizou na década de 60, quando a Letraset lançou decalques contendo passagens de Lorem Ipsum, e mais recentemente quando passou a ser integrado a softwares de editoração eletrônica como Aldus PageMaker.</p>
                </article>
                <article>
                    <a href="www.youtube.com/watch?v=N7u59leTuX4" data-lity>
                    <img src="img/grande_1.jpg" alt="Clínica Ki-Beleza">
                    </a>
                </article>
            </div>
        </section><!-- FIM SOBRE -->

        <section class="equipe wow fadeInUpBig"><!-- EQUIPE -->
			<div class="site">
				<h2>EQUIPE</h2>
				<hr>
				<article class="equipeUm">
					<article>
						<img src="img/galeria1.png" alt="XPTO 1">
						<h3>XPTO 1</h3>
						<h4>Massagista</h4>
					</article>
					<article>
						<img src="img/galeria2.png" alt="XPTO 2">
						<h3>XPTO 2</h3>
						<h4>Beleza e Moda</h4>
					</article>
					<article>
						<img src="img/galeria3.png" alt="XPTO 3">
						<h3>XPTO 3</h3>
						<h4>Profissional em estética</h4>
					</article>
					<article>
						<img src="img/galeria4.png" alt="XPTO 4">
						<h3>XPTO 4</h3>
						<h4>Cabeleireira</h4>
					</article>
					<article>
						<img src="img/galeria5.png" alt="XPTO 5">
						<h3>XPTO 5</h3>
						<h4>Massagista</h4>
					</article>
				</article>
			</div>
        </section><!-- FIM EQUIPE -->
        
        

        <?php require_once("rodape.php"); ?>

        
    </body>
    <script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>
    <script type="text/javascript" src="js/slick.js"></script>
    <script type="text/javascript" src="js/wow.js"></script>
	<script type="text/javascript" src="js/lity.js"></script>
    <script type="text/javascript" src="js/animacao.js"></script>
    <script src="https://cdn.shopify.com/s/files/1/0683/5883/t/3/assets/instafeed.min.js"></script>
    <script src="https://cdn.jsdelivr.net/jquery.slick/1.3.15/slick.min.js"></script>
    <script src="js/instafeed.js"></script>
</html>