<footer class="rodape wow fadeInUpBig"><!-- INICIO RODAPE-->
			<div class="site">
				<img src="img/logo-kibeleza-branco.svg" alt="Logo Clínica kiBeleza">
				<p>Lorem Ipsum é simplesmente uma simulação de texto da indústria tipográfica e de impressos. Lorem Ipsum é simplesmente uma simulação de texto da indústria tipográfica e de impressos. Lorem Ipsum é simplesmente uma simulação de texto da indústria tipográfica e de impressos. Lorem Ipsum é simplesmente uma simulação de texto da indústria tipográfica e de impressos.</p>
			     <ul class="icones-redes-sociais">
                    <li>
                        <a class="facebook" href="https://facebook.com" target="_blank" >
                            Facebook
                        </a>
                    </li>
                    <li>
                        <a class="twitter" href="https://twitter.com" target="_blank" >
                            Twitter
                        </a>
                    </li>
                    <li>
                        <a class="instagran" href="https://www.instagram.com/?hl=pt-br" target="_blank" >
                            Instagran
                        </a>
                    </li>
                </ul>
			</div>
		</footer><!--FIM INICIO RODAPE-->
		<h2 class="direitos">&copy; Todos os direitos reservados KiBeleza</h2>