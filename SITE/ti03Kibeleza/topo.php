<header id="topo-fixo">
            <div class="site topo">
                <h1>Clinica KiBeleza!!!</h1>
				<button class="abrir-menu"></button>
                <nav>
					<button class="fechar-menu"></button>
                    <ul>
                        <li>
                            <a href="index.php">
                                <span class="icon iconHome"></span>
                                <span>HOME</span>
                            </a>
                        </li>
                        <li>
                            <a href="sobre.php">
                                <span class="icon iconSobre"></span>
                                <span>SOBRE</span>
                            </a>
                        </li>
                        <li>
                            <a href="pg-servico.php">
                                <span class="icon iconServico"></span>
                                <span>SERVIÇO</span>
                            </a>
                        </li>
						 <li>
                            <a href="noticias/">
                                <span class="icon iconBlog"></span>
                                <span>BLOG</span>
                            </a>
                        </li>
						
                        <li>
                            <a href="contato.php">
                                <span class="icon iconContato"></span>	
                                <span>CONTATO</span>
                            </a>
                        </li>
                    </ul>
                </nav>
            </div>
        </header>

<script type="text/javascript" src="js/menu-fixo.js"></script>


